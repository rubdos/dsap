function [ Center, Surround ] = adaptivePlanning( XLeft, XRight )
    %UNTITLED Summary of this function goes here

    %initialize
    WLeft = zeros(1,length(XLeft)+1);
    WRight = zeros(1,length(XRight)+1);
    Center = zeros(1,length(XRight));
    Surround = zeros(1,length(XRight));

    mu = 10e-10;

    WLeft(1) = 1;
    WRight(1) = 1;

    for n=1:1:length(XLeft)
        X = [XLeft(n) XRight(n)].';
        W = [WLeft(n) WRight(n)].';
        Y = W.' * X;

        WLeft(n+1) = WLeft(n) - mu*Y*(XLeft(n)-WLeft(n)*Y);
        WRight(n+1) = WRight(n) - mu*Y*(XRight(n)-WRight(n)*Y);

        Center(n) = WLeft(n)*XLeft(n) + WRight(n)*XRight(n);
        Surround(n) = WRight(n)*XLeft(n) + WLeft(n)*XRight(n);
    end
end

