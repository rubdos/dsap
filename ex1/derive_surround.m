function [ RL, RR ] = derive_surround( surround, dt, sample_freq )
    %UNTITLED4 Summary of this function goes here
    %   Detailed explanation goes here
    if nargin < 3
        dt=0.012;
        sample_freq=44100;
    end
    sample_shift = sample_freq*dt;
    surround = padarray(surround,round(sample_shift),'pre');
    fir=fir1(120, 7e3/sample_freq);
    surround = filter(fir,1,surround);

    RL = imag(hilbert(surround));
    RR = -RL;
end

