function [Center,Surround] = LMS( Left, Right )
    %UNTITLED2 Summary of this function goes here
    %   Parameters
    N = 2205;
    mu = 1e-4;

    e = zeros(1,length(Right));
    y = zeros(1, length(Right));
    w = zeros(1,N);
    x = zeros(1,N);
    d = Left;

    for i = 1:length(Left)

        for j = 1:N
            if i-j+1<1
                x(j) = 0;
            else
                x(j) = Right(i-j+1);
            end
        end
        y(i) = w*x';
        e(i) = y(i) - d(i);
        w = w - 2*mu*e(i)*x;


    end
    Center = y';
    Surround = e';

end

