function [ center, lfe ] = filter_center( center, fs )
    f_center = 4e3;
    f_lfe = 200;
    fir_c=fir1(120, f_center/fs);
    fir_lfe=fir1(120, f_lfe/fs);
    center=filter(fir_c,1,center);
    lfe=filter(fir_lfe,1,center);
end

