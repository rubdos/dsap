function [ left, right, surround, center] = psdm( input )

    assert(size(input, 2) == 2)
    left = input(:,1);
    right = input(:,2);

    center = (left+right)/sqrt(2);
    surround = (left-right)/sqrt(2);
end

