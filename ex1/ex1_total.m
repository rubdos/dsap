% DSAP - Ex 1
% Jasper, Jarne, Ruben, Tristan

[y,Fs] = audioread('stereo2surround_testfile.wav');
Left = y(:,1);
Right = y(:,2);

%% Upmixing

%PSDM
[Left_PSDM, Right_PSDM, Surround_PSDM, Center_PSDM] = psdm(y);

%LMS
[Center_LMS, Surround_LMS] = LMS(Left, Right);

%PCA
[Center_PCA, Surround_PCA] = PCA_based(Left,Right,1);
Center_PCA = Center_PCA.';
Surround_PCA = Surround_PCA.';

%Adaptive Planning
[Center_AP, Surround_AP] = adaptivePlanning(Left, Right);
Center_AP = Center_AP.';
Surround_AP = Surround_AP.';

%% Filtering

[C_PSDM,LFE_PSDM] = filter_center(Center_PSDM,Fs);
[C_LMS,LFE_LMS] = filter_center(Center_LMS,Fs);
[C_PCA,LFE_PCA] = filter_center(Center_PCA,Fs);
[C_AP,LFE_AP] = filter_center(Center_AP,Fs);

[RL_PSDM, RR_PSDM] = derive_surround(Surround_PSDM);
[RL_LMS, RR_LMS] = derive_surround(Surround_LMS);
[RL_PCA, RR_PCA] = derive_surround(Surround_PCA);
[RL_AP, RR_AP] = derive_surround(Surround_AP);

%% Write to file

audiowrite('Center_PSDM.wav',[(C_PSDM/max(abs(C_PSDM)));(LFE_PSDM/max(abs(LFE_PSDM)))],Fs);
audiowrite('Rear_PSDM.wav',[(RL_PSDM/max(abs(RL_PSDM)));(RR_PSDM/max(abs(RR_PSDM)))],Fs);

audiowrite('Center_LMS.wav',[(C_LMS/max(abs(C_LMS)));(LFE_LMS/max(abs(LFE_LMS)))],Fs);
audiowrite('Rear_LMS.wav',[(RL_LMS/max(abs(RL_LMS)));(RR_LMS/max(abs(RR_LMS)))], Fs);

audiowrite('Center_PCA.wav',[(C_PCA/max(abs(C_PCA)));(LFE_PCA/max(abs(LFE_PCA)))],Fs);
audiowrite('Rear_PCA.wav',[(RL_PCA/max(abs(RL_PCA)));(RR_PCA/max(abs(RR_PCA)))],Fs);

audiowrite('Center_AP.wav',[(C_AP/max(abs(C_AP)));(LFE_AP/max(abs(LFE_AP)))],Fs);
audiowrite('Rear_AP.wav',[(RL_AP/max(abs(RL_AP)));(RR_AP/max(abs(RR_AP)))],Fs);
