function [ Center,Surround ] = PCA_based( L,R,f )
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here

    N = 1024;
    M = 128;
    n1 = 1:M;
    y1 = sin(pi*n1/2/M);
    y2 = sin(pi*n1/2/M+pi/2);
    window = [y1 ones(1,N-2*M) y2];
    %plot(window);
    Center = zeros(1,length(L));
    Surround = zeros(1,length(L));
    cl_prev = 0;
    cr_prev = 0;
    sl_prev = 0;
    sr_prev = 0;
    for i = 1:N-M:length(L)-N
        L_wind = L(i:i+N-1)'.*window.^2;
        R_wind = R(i:i+N-1)'.*window.^2;
        A = cov(L_wind,R_wind);
        [V,D] = eig(A);
        if (D(1,1)>=D(2,2))
            cl = V(1,1);
            cr = V(2,1);
            sl = V(1,2);
            sr = V(2,2);
        else
            cl = V(1,2);
            cr = V(2,2);
            sl = V(1,1);
            sr = V(2,1);
        end
        cl = f*cl + (1-f)*cl_prev;
        cr = f*cr + (1-f)*cr_prev;
        sl = f*sl + (1-f)*sl_prev;
        sr = f*sr + (1-f)*sr_prev;
        Center(i:i+N-1) = Center(i:i+N-1)+ cl*L_wind + cr*R_wind;
        Surround(i:i+N-1) = Surround(i:i+N-1)+sl*L_wind + sr*R_wind;
        cl_prev = cl;
        cr_prev = cr;
        sl_prev = sl;
        sr_prev = sr;
    end
end

