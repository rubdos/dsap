%DSAP - Ex2
%Jasper Bevernage, Ruben De Smet, Tristan Hoogewys, Jarne Verelst

%clear all;
%close all;
%clc;
%% Read files

%Mic 3 in front, 4 in back
%Mic 1 right, 2 left
[Mic,Fs] = audioread('scenario_1.flac');


%% Settings
Tw = 0.5; %Length of window in seconds

%% Process
Tws = floor(Tw*Fs)+1; %Length of window in samples
hannW = hann(Tws);
ThetaList = zeros(1,length(Mic));
Theta = zeros(1,length(1:round((Tws-1)/2):length(Mic)-Tws));
ThetaCoor = Theta;
counter=1;
%Calculate angle
for i = 1:round((Tws-1)/2):length(Mic)-Tws
    Mic1 = Mic(i:i+Tws-1,7).*hannW; %Mic 1 is left
    Mic2 = Mic(i:i+Tws-1,4).*hannW; %Mic 2 is right
    Mic3 = Mic(i:i+Tws-1,5).*hannW; %Mic 3 is back
    Mic4 = Mic(i:i+Tws-1,1).*hannW; %Mic 4 is front
    
    Theta(counter) = TDOAToTheta( Fs, Mic1, Mic2, Mic3, Mic4);
    ThetaCoor(counter) = i;
        
    ThetaList(i:i+round((Tws-1)/2)-1) = repmat(Theta(counter),1,round((Tws-1)/2));
    
    counter=counter+1;
end
ThetaListInter = interp1(ThetaCoor,Theta,1:length(Mic)-Tws);
%Make sound signals

test_output = project_sound(Mic, ThetaListInter);


%Reverbs


