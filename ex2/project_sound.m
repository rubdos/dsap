function [output] = project_sound(sound, phi)
    % Define channels
    available_channels=[
     1 -1;
     1  1;
    -1  1;
    -1 -1;
    ];

    channel_distances = sqrt(sum(available_channels.*available_channels,2));
    channel_unit_vectors = available_channels./channel_distances;

    channels_angles=[atan2(available_channels(:,2), available_channels(:,1))];
    channels_angles=wrapTo2Pi(channels_angles);

    %%% Normal vector for sound source
    p = [cos(phi); sin(phi)];

    % Find closest channel for each phi
    diff = repmat(channels_angles, 1, length(phi)) - repmat(phi, length(channels_angles), 1);
    diff = abs(wrapToPi(diff));

    %%% Initialise channels
    output = zeros(size(available_channels, 1), length(phi));

    for sample = 1:length(phi)
        [~, channel_1_id] = min(diff(:, sample));
        % calculate next closest channel
        channel_cw = channel_1_id - 1;
        channel_ccw = channel_1_id + 1;
        % wrap around channel id's
        if channel_ccw < 1
            channel_ccw = length(available_channels);
        end
        if channel_ccw > length(available_channels)
            channel_ccw = 1;
        end
        if channel_cw < 1
            channel_cw = length(available_channels);
        end
        if channel_cw > length(available_channels)
            channel_cw = 1;
        end
        channel_2_id = channel_cw;
        if diff(channel_cw, sample) > diff(channel_ccw, sample)
            channel_2_id = channel_ccw;
        end
        channel_1 = available_channels(channel_1_id, :);
        channel_2 = available_channels(channel_2_id, :);

        %%% Normalise vectors for both speakers
        channel_1 = channel_1/norm(channel_1);
        channel_2 = channel_2/norm(channel_2);

        gains = p(:,sample)'*[channel_1; channel_2]^-1;
        gains = gains/norm(gains);

        channel_1 = sound(sample) * gains(1);
        channel_2 = sound(sample) * gains(2);
        output(channel_1_id, sample) = channel_1;
        output(channel_2_id, sample) = channel_2;
    end
end
