function [ theta ] = TDOAToTheta( Fs , Mic1 , Mic2 , Mic3 , Mic4 )
%TDOA to Theta for 5.1 sound reconstruction

%Calculate number of sec Mic2/4 is lagging behind Mic1/3 (see doc xcorr)
[acor12,lag] = xcorr(Mic1,Mic2);
[~,I] = max(abs(acor12));
lagDiff12 = lag(I);
TDOA12 = lagDiff12/Fs; 

[acor34,lag] = xcorr(Mic3,Mic4);
[~,I] = max(abs(acor34));
lagDiff34 = lag(I);
TDOA34 = lagDiff34/Fs; 

%Calculate theta (in rad), sigma not needed
if TDOA12 > 0 && TDOA34 >= 0
    theta = 3*pi/2 + atan(TDOA34/TDOA12);
elseif TDOA12 <= 0 && TDOA34 > 0
    theta = atan(-TDOA12/TDOA34);
elseif TDOA12 < 0 && TDOA34 <= 0
    theta = pi/2 + atan((-TDOA34)/(-TDOA12));
elseif TDOA12 >= 0 && TDOA34 < 0
    theta = pi + atan(TDOA12/(-TDOA34));
else
    theta = 0;
    disp('Sound coming from above');
end

end

